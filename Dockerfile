FROM registry.gitlab.com/yshukevich-examples/infrastructure/template-java:1.0.64

USER root

ARG MAVEN_VERSION=3.9.9
ARG MAVEN_DOWNLOAD_SHA512=8beac8d11ef208f1e2a8df0682b9448a9a363d2ad13ca74af43705549e72e74c9378823bf689287801cbbfc2f6ea9596201d19ccacfdfb682ee8a2ff4c4418ba
ARG BASE_URL=https://dlcdn.apache.org/maven/maven-3/${MAVEN_VERSION}/binaries
ENV PATH /usr/share/maven/apache-maven-${MAVEN_VERSION}/bin:${PATH}
ENV MAVEN_CONFIG "~/.m2"

RUN mkdir -p /usr/share/maven \
  && wget --no-verbose --output-document=maven.zip "${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.zip" \
  && echo "${MAVEN_DOWNLOAD_SHA512}  maven.zip" | sha512sum -c - \
  && unzip maven.zip -d /usr/share/maven \
  && cd /usr/share/maven/apache-maven-${MAVEN_VERSION} \
  && rm -f maven.zip \
  && ln -s /usr/share/maven/apache-maven-${MAVEN_VERSION}/bin/mvn /usr/bin/mvn \
  && echo "Testing Maven installation" \
  && mvn --version

USER java
CMD ["mvn"]
